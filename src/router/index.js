import { createRouter, createWebHistory } from 'vue-router'
import Homepage from '../components/home/Homepage.vue'
import Registerpage from '../components/register/Registerpage.vue'
import Success from '../components/register/Success.vue';
import Searchpage from '../components/gBooks/Searchpage.vue';
import Librarypage from '../components/library/Librarypage.vue';

const routes = [
  {
    path: '/',
    name: 'Homepage',
    component: Homepage
  },
  {
    path: '/register',
    name: 'Registerpage',
    component: Registerpage
  },
  {
    path: '/success',
    name: 'Success',
    component: Success
  },
  {
    path: '/search',
    name: 'Searchpage',
    component: Searchpage
  },
  {
    path: '/library',
    name: 'Librarypage',
    component: Librarypage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
